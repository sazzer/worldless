module.exports = {
  verbose: true,
  roots: ["./src"],
  transform: {
    "^.+\\.ts$": "ts-jest"
  }
};
