import { truncateTable, seedTable, requester } from '@worldless/acceptance-lib';

describe('Get World By Slug', () => {
    beforeEach(async () => {
        await truncateTable('worlds', 'worldsTable');
    });

    it('Returns correctly when getting a known world', async () => {
        await seedTable('worlds', 'worldsTable', {
            slug: 'idoexist',
            name: 'My World',
            owner: 'myOwner',
        });

        const result = await requester('worlds').get('/worlds/idoexist');

        expect(result.status).toEqual(200);
        expect(result.headers['content-type']).toEqual('application/json');
        expect(result.data).toMatchSnapshot();
    });

    it('Returns correctly when getting an unknown world', async () => {
        const result = await requester('worlds').get('/worlds/idontexist');

        expect(result.status).toEqual(404);
        expect(result.headers['content-type']).toEqual('application/problem+json');
        expect(result.data).toMatchSnapshot();
    });
});
