import axios from 'axios';
import { Resources } from './resources';

/**
 * Construct an Axios instance that can make requests to the given stack
 * @param stack  the name of the stack
 * @return the Axios instance to use
 */
export function requester(stack: string) {
    const resources = new Resources();
    const baseUrl = resources.getResource(stack, 'ServiceEndpoint');

    return axios.create({
        baseURL: baseUrl,
        timeout: 3000,
        validateStatus: () => true,
    });
}
