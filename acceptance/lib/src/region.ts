/** The default AWS Region to use */
const DEFAULT_REGION = 'us-east-1';

/**
 * Determine the AWS Region to work with
 */
export function determineRegion() {
    return process.env.AWS_REGION || DEFAULT_REGION;
}
