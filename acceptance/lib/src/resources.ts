import { safeLoad } from 'js-yaml';
import { readFileSync } from 'fs';

/**
 * Wrapper around the resources for the stacks
 */
export class Resources {
    /** The cached stack data that we've loaded */
    private stacks: { [stack: string]: { [key: string]: string } } = {};

    /**
     * Get a stack resource for use in the tests
     * @param stack the stack to get the resource for
     * @param resource the identifier of the resource
     * @return the resource value
     */
    public getResource(stack: string, resource: string): string {
        let stackData: { [key: string]: string };

        if (this.stacks[stack]) {
            stackData = this.stacks[stack];
        } else {
            stackData = safeLoad(readFileSync(`../../stacks/${stack}/.serverless/stack-output/outputs.yml`, 'utf-8'));
            this.stacks[stack] = stackData;
        }

        if (!stackData[resource]) {
            throw new Error(`Resource ${resource} not found in stack output ${stack}`);
        }

        return stackData[resource];
    }
}
