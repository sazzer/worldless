import * as AWS from 'aws-sdk';
import { Resources } from './resources';
import { determineRegion } from './region';
/**
 * Truncate an entire DynamoDB table
 * @param stack  the name of the stack
 * @param resource the name of the DynamoDB resource in the stack
 */
export async function truncateTable(stack: string, resource: string) {
    // Load our serverless resources so that we can get the name of the table to truncate
    const resources = new Resources();
    const table = resources.getResource(stack, resource);
    const region = determineRegion();

    const baseParams = {
        TableName: table,
    };

    console.log('Truncating table: ', table, ' in region: ', region);

    const dynamoDb = new AWS.DynamoDB({ region });

    // Build a list of all the keys in the table
    const tableDescription = await dynamoDb.describeTable(baseParams).promise();
    if (tableDescription === undefined || tableDescription.Table === undefined) {
        throw new Error('Table not found: ' + table);
    }
    if (tableDescription.Table.KeySchema === undefined) {
        throw new Error('Unable to truncate table with no key schema: ' + table);
    }
    const keys = tableDescription.Table.KeySchema.map((key) => key.AttributeName);

    // Get a full list of every record in the table
    const data = await dynamoDb.scan(baseParams).promise();
    if (data.Items === undefined) {
        return;
    }

    // For every record in the table, we now need to delete it
    // DynamoDB doesn't do bulk delete, so we need to do it one at a time
    // We also need to only provide the keys - too much causes problems
    const allDeletes = data.Items
        .map((item) => {
            const result: { [key: string]: any } = {};
            keys.forEach((key) => {
                result[key] = item[key];
            });
            return result;
        })
        .map((item) => {
            console.log('Deleting record: ', item);
            return dynamoDb.deleteItem({
                TableName: table,
                Key: item,
            }).promise();
        });

    // And finally, wait for all the deletes to actually finish
    await Promise.all(allDeletes);
}

/**
 * Populate a record into a DynamoDB table
 * @param stack  the name of the stack
 * @param resource the name of the DynamoDB resource in the stack
 * @param record the record to populate into the table
 */
export async function seedTable(stack: string, resource: string, record: { [field: string]: any }) {
    // Load our serverless resources so that we can get the name of the table to truncate
    const resources = new Resources();
    const table = resources.getResource(stack, resource);

    const baseParams = {
        TableName: table,
    };

    const dynamoDb = new AWS.DynamoDB.DocumentClient({ region: determineRegion() });

    // Put the record into the table.
    // Note that this is a standard Put, so it will actually update an existing record if the keys match
    await dynamoDb.put({
        Item: record,
        ...baseParams,
    }).promise();

}
