#!/bin/sh

echo ===== Removing Node Modules ===
rm -rf node_modules
rm -rf stacks/*/node_modules
rm -rf acceptance/*/node_modules

echo ===== Removing Compiled Code =====
rm -rf stacks/*/dist

echo ===== Removing Serverless Cache =====
rm -rf stacks/*/.serverless

echo ===== Root Install =====
yarn install

echo ===== Running Tests =====
yarn test

