const gulp = require("gulp");
const serverlessGulp = require("serverless-gulp");
var args = require("yargs").argv;

/** The stack paths that we want to work with */
const paths = {
  serverless: ["gateway", "worlds"].map(p => `stacks/${p}/serverless.yml`)
};

/** The serverless stage to work with */
const stage = args.stage || "dev";

/** The region to work with */
const region = process.env.AWS_REGION || "us-east-1";

gulp.task("deploy", () => {
  return gulp
    .src(paths.serverless, { read: false })
    .pipe(serverlessGulp.exec("deploy", { stage: stage, region: region }));
});

gulp.task("remove", () => {
  return gulp
    .src(paths.serverless.reverse(), { read: false })
    .pipe(serverlessGulp.exec("remove", { stage: stage, region: region }));
});
