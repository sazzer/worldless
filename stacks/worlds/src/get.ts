import { Problem, Response } from '@worldless/response-lib';
import * as AWS from 'aws-sdk';

/**
 * Get a single World from the DynamoDB store
 * @param event The incoming event to process
 * @return A promise for the world details, or else for the error
 */
export async function get(event: any) {
    /** The Table name to work with */
    const table: string = process.env.DYNAMODB_TABLE || '';

    /** The DynamoDB Client */
    const dynamoDb = new AWS.DynamoDB.DocumentClient();

    /** The parameters to the DynamoDB query */
    const params = {
        TableName: table,
        Key: {
            slug: event.pathParameters.slug,
        },
    };

    try {
        const result: AWS.DynamoDB.DocumentClient.GetItemOutput = await dynamoDb.get(params).promise();

        if (result.Item) {
            return new Response({
                slug: result.Item.slug,
                name: result.Item.name,
                owner: result.Item.owner,
            }).build();
        } else {
            return new Problem('tag:nuworlds,2019:worlds/problems/not-found',
                'The requested world could not be found',
                404)
                .withExtraData('slug', event.pathParameters.slug)
                .build();
        }
    } catch (e) {
        console.log('Failed to get world', e);
        return new Problem('tag:nuworlds,2019:worlds/problems/unexpected-error',
            'An unexpected error occurred',
            500)
            .withExtraData('slug', event.pathParameters.slug)
            .build();
    }
}
