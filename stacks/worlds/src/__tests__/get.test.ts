import * as AWSMock from 'aws-sdk-mock';
import * as AWS from 'aws-sdk';
import { GetItemInput } from 'aws-sdk/clients/dynamodb';
import * as testSubject from '../get';

describe('Get World By Slug', () => {
    /** The environment from before the test */
    const OLD_ENV = process.env;

    /** Before we run the tests, ensure that the environment is as we need it to be */
    beforeAll(() => {
        jest.resetModules();
        process.env = {
            DYNAMODB_TABLE: 'worldsTable',
            ...OLD_ENV,
        };
    });

    /** Restore the environment after the test */
    afterAll(() => {
        process.env = OLD_ENV;
    });

    /** Set up the AWS Mock before our tests */
    beforeEach(() => {
        AWSMock.setSDKInstance(AWS);
    });

    /** Restore the DynamoDB mock back to standard behaviour */
    afterEach(() => {
        AWSMock.restore('DynamoDB.DocumentClient');
    });

    it('Responds correctly when the world is found', async () => {
        AWSMock.mock('DynamoDB.DocumentClient', 'get', (params: GetItemInput,
            callback: (error: any, success: any) => void) => {

            expect(params.TableName).toEqual('worldsTable');
            expect(params.Key).toEqual({ slug: 'myworld' });

            callback(null, {
                Item: {
                    slug: 'myworld',
                    name: 'My World',
                    owner: 'myOwner',
                },
            });
        });

        const response = await testSubject.get({
            pathParameters: {
                slug: 'myworld',
            },
        });
        expect(response).toMatchSnapshot();
    });

    it('Responds correctly when the world is not found', async () => {
        AWSMock.mock('DynamoDB.DocumentClient', 'get', (params: GetItemInput,
            callback: (error: any, success: any) => void) => {

            expect(params.TableName).toEqual('worldsTable');
            expect(params.Key).toEqual({ slug: 'myworld' });

            callback(null, {});
        });

        const response = await testSubject.get({
            pathParameters: {
                slug: 'myworld',
            },
        });
        expect(response).toMatchSnapshot();
    });
});
