/**
 * Representation of a response from an AWS handler
 */
export class Response {
    /** The response payload */
    private payload?: any;

    /** The status code */
    private status: number;

    /** The response headers */
    private headers: { [header: string]: string };

    /**
     * Construct the response
     * @param payload the response payload
     */
    constructor(payload?: any) {
        this.payload = payload;
        // If we have a payload then the default status code is a 200 OK.
        // If we don't have a payload then the default status code is a 204 No Content
        this.status = payload === undefined ? 204 : 200;
    }

    /**
     * Specify the status code to use
     * @param status The status code to use
     */
    public withStatusCode(status: number) {
        this.status = status;
        return this;
    }

    /**
     * Add a header to the response
     * @param header the header key
     * @param value the header value
     */
    public withHeader(header: string, value: string) {
        this.headers[header] = value;
        return this;
    }

    /**
     * Build an AWS Response object for this instance
     */
    public build() {
        return {
            statusCode: this.status,
            headers: this.headers,
            body: this.payload === undefined ? undefined : JSON.stringify(this.payload),
        };
    }
}
