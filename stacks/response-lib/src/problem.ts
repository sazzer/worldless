/**
 * Representation of an RFC-7807 Problem to return over the API
 */
export class Problem {
    /** The Problem type */
    private type: string;

    /** The Problem title */
    private title: string;

    /** The Problem status code */
    private status: number;

    /** The Problem Instance */
    private instance?: string;

    /** The Problem Detail message */
    private detail?: string;

    /** Any extra data for the problem */
    private extraData: { [key: string]: any } = {};

    /**
     * Construct the problem
     * @param type The type
     * @param title The title
     * @param status The status code
     */
    constructor(type: string, title: string, status: number) {
        this.type = type;
        this.title = title;
        this.status = status;
    }

    /**
     * Add a detail message to the problem
     * @param detail the detail message
     */
    public withDetail(detail: string) {
        this.detail = detail;
        return this;
    }

    /**
     * Add an instance value to the problem
     * @param instance The instance value
     */
    public withInstance(instance: string) {
        this.instance = instance;
        return this;
    }

    /**
     * Add some extra data to the problem
     * @param key the key for the extra data
     * @param value the value for the extra data
     */
    public withExtraData(key: string, value: any) {
        this.extraData[key] = value;
        return this;
    }

    /**
     * Build an AWS Response object for this problem
     */
    public build() {
        return {
            statusCode: this.status,
            headers: {
                'content-type': 'application/problem+json',
            },
            body: JSON.stringify({
                type: this.type,
                title: this.title,
                status: this.status,
                instance: this.instance,
                detail: this.detail,
                ...this.extraData,
            }),
        };
    }
}
